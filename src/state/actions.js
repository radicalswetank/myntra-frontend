import axios from "axios";

const FETCH_PRODUCTS_PENDING = "FETCH_PRODUCTS_PENDING";
const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";

const SEARCH_PRODUCTS_PENDING = "SEARCH_PRODUCTS_PENDING";
const SEARCH_PRODUCTS_SUCCESS = "SEARCH_PRODUCTS_SUCCESS";
const SEARCH_PRODUCTS_ERROR = "SEARCH_PRODUCTS_ERROR";

const FILTER_PRODUCTS_PENDING = "FILTER_PRODUCTS_PENDING";
const FILTER_PRODUCTS_SUCCESS = "FILTER_PRODUCTS_SUCCESS";
const FILTER_PRODUCTS_ERROR = "FILTER_PRODUCTS_ERROR";

function fetchProductsPending() {
  return {
    type: FETCH_PRODUCTS_PENDING,
  };
}

function fetchProductsSuccess(products) {
  return {
    type: FETCH_PRODUCTS_SUCCESS,
    payload: products.products,
  };
}

function fetchProductsError(error) {
  return {
    type: FETCH_PRODUCTS_ERROR,
    error: error,
  };
}

function fetchProductsAction() {
  return (dispatch) => {
    dispatch(fetchProductsPending());
    console.log("Running fetchProductsAction");
    axios
      .get("https://demo7242716.mockable.io/products")
      .then((res) => {
        //console.log(res.data);
        dispatch(fetchProductsSuccess(res.data));
      })
      .catch((error) => {
        dispatch(fetchProductsError(error));
      });
  };
}

function searchProductsPending() {
  return {
    type: SEARCH_PRODUCTS_PENDING,
  };
}

function searchProductsSuccess(products) {
  return {
    type: SEARCH_PRODUCTS_SUCCESS,
    payload: products,
  };
}

function searchProductsError(error) {
  return {
    type: SEARCH_PRODUCTS_ERROR,
    error: error,
  };
}

function searchProductsAction(data) {
  return (dispatch) => {
    dispatch(searchProductsSuccess(data));
  };
}

function filterProductsPending() {
  return {
    type: FILTER_PRODUCTS_PENDING,
  };
}

function filterProductsSuccess(products) {
  return {
    type: FILTER_PRODUCTS_SUCCESS,
    payload: products,
  };
}

function filterProductsError(error) {
  return {
    type: FILTER_PRODUCTS_ERROR,
    error: error,
  };
}

function filterProductsAction(data) {
  return (dispatch) => {
    dispatch(filterProductsSuccess(data));
    //console.log(data);
  };
}

export {
  fetchProductsAction,
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
  searchProductsAction,
  SEARCH_PRODUCTS_PENDING,
  SEARCH_PRODUCTS_SUCCESS,
  SEARCH_PRODUCTS_ERROR,
  filterProductsAction,
  FILTER_PRODUCTS_PENDING,
  FILTER_PRODUCTS_SUCCESS,
  FILTER_PRODUCTS_ERROR,
};
