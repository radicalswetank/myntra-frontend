import { combineReducers } from "redux";
import {
  FETCH_PRODUCTS_PENDING,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
  SEARCH_PRODUCTS_PENDING,
  SEARCH_PRODUCTS_SUCCESS,
  SEARCH_PRODUCTS_ERROR,
  FILTER_PRODUCTS_SUCCESS,
} from "./actions";

const initialState = {
  pending: false,
  products: [],
  error: null,
};
// const renderData = useSelector((state) => state);

//const filteredState;

export function allProductsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCTS_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: action.payload,
      };
    case FETCH_PRODUCTS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    case SEARCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: state.products,
      };
    case FILTER_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: state.products,
      };

    default:
      return state;
  }
}

export function displayProductsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCTS_PENDING:
      return {
        ...state,
        pending: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: action.payload,
      };
    case FETCH_PRODUCTS_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      };
    case SEARCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: action.payload,
      };
    case FILTER_PRODUCTS_SUCCESS:
      return {
        ...state,
        pending: false,
        products: action.payload,
      };

    default:
      return state;
  }
}

const getProducts = (state) => state.products;
const getProductsPending = (state) => state.pending;
const getProductsError = (state) => state.error;

const rootReducer = combineReducers({
  allProducts: allProductsReducer,
  displayProducts: displayProductsReducer,
});

export { getProducts, getProductsPending, getProductsError, rootReducer };
