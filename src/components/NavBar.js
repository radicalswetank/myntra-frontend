import React from "react";
import { AppBar, Toolbar } from "@material-ui/core";
import MyntraLogo from "../assets/myntra.png";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import { alpha, makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ProfileIcon from "@material-ui/icons/PersonOutlineOutlined";
import WishListIcon from "@material-ui/icons/TurnedInNotOutlined";
import BagIcon from "@material-ui/icons/LocalMallOutlined";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import {
  getProductsPending,
  getProducts,
  getProductsError,
} from "../state/reducers";
import { searchProductsAction } from "../state/actions";
import { useDispatch, useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: "#E2DDDA",
    padding: theme.spacing(0, 20, 0, 0),

    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(12),
      marginRight: theme.spacing(2),
    },
    display: "flex",
    flexDirection: "row",
    width: "20vw",
  },
  searchIcon: {
    padding: theme.spacing(1, 2),
    height: "100%",
    position: "sticky",
    pointerEvents: "none",
    display: "flex",

    justifyContent: "center",
  },
  titleLogo: {
    maxHeight: "10vh",
  },
  toolbar: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sections: {
    display: "flex",
    flexDirection: "row",

    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(8),
  },
  sectionItems: {
    display: "flex",
    marginLeft: theme.spacing(4),
  },
  logo: {
    display: "flex",
  },
  icons: {
    display: "flex",
    padding: theme.spacing(0, 2),
    marginLeft: "2.5vw",
  },
  iconsItems: {
    display: "flex",
    flexDirection: "column",
    fontSize: theme.spacing(2),
    margin: theme.spacing(1),
    alignItems: "center",
  },
}));

function NavBar() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const renderData = useSelector((state) => state);

  const handleSearch = (event) => {
    //    console.log(typeof event.target.value);
    // dispatch(e.target.value);
    //console.log(renderData.displayProducts.products);
    let newData = [];
    renderData.allProducts.products.map((x) => {
      if (
        x.productName.toLowerCase().includes(event.target.value.toLowerCase())
      ) {
        // console.log(x);
        newData.push(x);
      }
    });
    //console.log(newData);
    dispatch(searchProductsAction(newData));
  };
  const [inputText, setInputText] = React.useState("");
  const handleChange = (event) => {
    setInputText(event);
  };
  return (
    <AppBar position="sticky" color="inherit">
      <Toolbar>
        <div className={classes.toolbar}>
          <div className={classes.logo}>
            <img
              src={MyntraLogo}
              className={classes.titleLogo}
              alt="myntra-logo"
            />
          </div>
          <div className={classes.sections}>
            <div>
              <Typography className={classes.title} variant="h6" noWrap>
                MEN
              </Typography>
            </div>
            <div className={classes.sectionItems}>
              <Typography className={classes.title} variant="h6" noWrap>
                WOMEN
              </Typography>
            </div>
            <div className={classes.sectionItems}>
              <Typography className={classes.title} variant="h6" noWrap>
                KIDS
              </Typography>
            </div>
            <div className={classes.sectionItems}>
              <Typography className={classes.title} variant="h6" noWrap>
                HOME & LIVING
              </Typography>
            </div>
            <div className={classes.sectionItems}>
              <Typography className={classes.title} variant="h6" noWrap>
                OFFERS
              </Typography>
            </div>
          </div>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              fullWidth
              placeholder="Search for products,brands and more"
              onKeyPress={(e) => e.key === "Enter" && handleSearch(e)}
              value={inputText}
              onChange={(e) => handleChange(e.target.value)}
            />
          </div>
          <div className={classes.icons}>
            <div className={classes.iconsItems}>
              <IconButton color="inherit">
                <ProfileIcon />
              </IconButton>
              <div>Profile</div>
            </div>
            <div className={classes.iconsItems}>
              <IconButton color="inherit">
                <WishListIcon />
              </IconButton>
              <div>Wishlist</div>
            </div>
            <div className={classes.iconsItems}>
              <IconButton color="inherit">
                <BagIcon />
              </IconButton>
              <div>Bag</div>
            </div>
          </div>
        </div>
      </Toolbar>
    </AppBar>
  );
}

/*const mapStateToProps = (state) => ({
  error: getProductsError(state),
  products: getProducts(state),
  pending: getProductsPending(state),
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProductsAction: () => dispatch(fetchProductsAction()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
*/
export default NavBar;
