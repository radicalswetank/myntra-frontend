import React from "react";
import Typography from "@material-ui/core/Typography";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";
import FormHelperText from "@material-ui/core/FormHelperText";
import Box from "@material-ui/core/Box";
import { useDispatch, useSelector } from "react-redux";
import { filterProductsAction } from "../state/actions";

const useStyles = makeStyles((theme) => ({
  filterBar: {
    width: "15vw",
    padding: "2.5vw",
    margin: "2.5vw",
  },
}));

function FilterMenu() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const renderData = useSelector((state) => state);

  const [gender, setGender] = React.useState("");
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedF: true,
    checkedG: true,
  });
  const [state2, setState2] = React.useState({
    gilad: true,
    jason: false,
    antoine: false,
  });
  const handleGender = (event) => {
    setGender(event.target.value);
    let newData = [];
    renderData.allProducts.products.map((x) => {
      if (x.gender.toLowerCase().includes(event.target.value.toLowerCase())) {
        newData.push(x);
      }
    });
    //console.log(newData);
    dispatch(filterProductsAction(newData));
  };
  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const handleChange2 = (event) => {
    setState2({ ...state2, [event.target.name]: event.target.checked });
  };
  const { gilad, jason, antoine } = state;

  return (
    <div>
      <Box className={classes.filterBar} sx={{ border: "1px solid grey" }}>
        <Typography>Filter</Typography>
        <div>
          <FormControl component="fieldset">
            <RadioGroup
              aria-label="gender"
              name="gender1"
              value={gender}
              onChange={handleGender}
            >
              <FormControlLabel control={<Radio />} label="Men" value="men" />
              <FormControlLabel
                control={<Radio />}
                label="Women"
                value="women"
              />
              <FormControlLabel
                control={<Radio />}
                label="Unisex"
                value="Unisex"
              />
              <FormControlLabel
                control={<Radio />}
                label="Girls"
                value="girls"
              />
            </RadioGroup>
          </FormControl>
        </div>
      </Box>
      <Box className={classes.filterBar} sx={{ border: "1px solid grey" }}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Categories</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={gilad}
                  onChange={handleChange2}
                  name="gilad"
                />
              }
              label="Gilad Gray"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={jason}
                  onChange={handleChange2}
                  name="jason"
                />
              }
              label="Jason Killian"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={antoine}
                  onChange={handleChange2}
                  name="antoine"
                />
              }
              label="Antoine Llorca"
            />
          </FormGroup>
          <FormHelperText>+277 More</FormHelperText>
        </FormControl>
      </Box>
      <Box className={classes.filterBar} sx={{ border: "1px solid grey" }}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Brand</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  checked={gilad}
                  onChange={handleChange2}
                  name="Puma"
                />
              }
              label="Puma"
            />
            <FormControlLabel
              control={
                <Checkbox checked={jason} onChange={handleChange2} name="H&M" />
              }
              label="H&M"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={antoine}
                  onChange={handleChange2}
                  name="Adidas"
                />
              }
              label="Adidas"
            />
          </FormGroup>
          <FormHelperText>+277 More</FormHelperText>
        </FormControl>
      </Box>
    </div>
  );
}

export default FilterMenu;
