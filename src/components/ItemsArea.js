import axios from "axios";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import { useState, useEffect } from "react";
import ItemCard from "./ItemCard";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import { fetchProductsAction } from "../state/actions";
import { connect } from "react-redux";
import {
  getProductsPending,
  getProducts,
  getProductsError,
} from "../state/reducers";
import { bindActionCreators } from "redux";
import Typography from "@material-ui/core/Typography";

import { useSelector, useDispatch } from "react-redux";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  root: {
    maxWidth: 245,
  },
  media: {
    aspectRatio: "4/5",
  },
  box: {
    maxWidth: "80vw",
    paddingTop: "1vh",
    justifyContent: "flex-",
  },
});

function ItemsArea() {
  const classes = useStyles();

  const renderData = useSelector((state) => state);
  const dispatch = useDispatch();
  useEffect(() => {
    if (!renderData.allProducts.products.length) {
    }
    dispatch(fetchProductsAction());
  }, []);
  /*
  
  
  const [imagesData, setImagesData] = useState(null);
  async function getData(prop) {
    //console.log("fetching");
    await axios.get(prop).then((response) => response.data);
  }
   useEffect(() => {
    {
      fetchImages();
      async function fetchImages() {
        const imageArray = [];
        try {
          const res = renderData.products.map((x) => getData(x.searchImage));
          console.log("Restype", typeof res);
          console.log("res", res);
          imageArray.push(res);
          console.log("Array", imageArray);
          setImagesData(imageArray);
          //console.log("Res", res);
        } catch (err) {
          console.log(err);
        }
      }
    }
  }, [renderData]);*/
  return (
    <div className={classes.box}>
      <Box>
        <Grid
          container
          //spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          {renderData &&
            renderData.displayProducts.products.map((x) => (
              //<img src={x.searchImage} />
              <Card className={classes.root}>
                <CardActionArea>
                  <CardMedia
                    className={classes.media}
                    image={x.searchImage}
                    title="Sample"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h6">
                      {x.product}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {x.price}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))}
        </Grid>
      </Box>
    </div>
  );
}
/*
const mapStateToProps = (state) => ({
  error: getProductsError(state),
  products: getProducts(state),
  pending: getProductsPending(state),
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProductsAction: () => dispatch(fetchProductsAction()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ItemsArea);
*/
export default ItemsArea;
