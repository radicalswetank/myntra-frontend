import NavBar from "./components/NavBar";
import { alpha, makeStyles } from "@material-ui/core/styles";
import FilterMenu from "./components/FilterMenu";
import ItemsArea from "./components/ItemsArea";
import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Provider } from "react-redux";
import {
  getProductsPending,
  getProducts,
  getProductsError,
} from "./state/reducers";
import store from "./state/store";
import fetchProducts from "./state/actions";

const useStyles = makeStyles((theme) => ({
  commonArea: {
    display: "flex",
    flexDirection: "row",
      
  },
}));

function App() {
  const classes = useStyles();
  /*const apiUrl = `https://demo7242716.mockable.io/products`;
  const [renderData, setRenderData] = useState(null);
  
  useEffect(() => {
    getData();

    async function getData() {
      try {
        const response = await axios.get(apiUrl);
        setRenderData(response.data);
      } catch (err) {
        console.log(err);
      }
    }
  }, []);*/

  return (
    <Provider store={store}>
      <div>
        <div>
          <NavBar />
        </div>
        <div className={classes.commonArea}>
          <div>
            <FilterMenu />
          </div>

          <ItemsArea />
        </div>
      </div>
    </Provider>
  );
}
/*const mapStateToProps = (state) => ({
  error: getProductsError(state),
  products: getProducts(state),
  pending: getProductsPending(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      fetchProducts: fetchProductsAction,
    },
    dispatch
  );
*/
export default App;
